Source: pgaudit-1.7
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders: Michael Banck <mbanck@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 libkrb5-dev,
 libssl-dev,
 postgresql-all (>= 217~),
Standards-Version: 4.6.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/postgresql/pgaudit
Vcs-Git: https://salsa.debian.org/postgresql/pgaudit.git -b master
Homepage: http://pgaudit.org/

Package: postgresql-PGVERSION-pgaudit
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, postgresql-PGVERSION
Description: PostgreSQL Audit Extension
 The pgAudit extension provides detailed session and/or object audit logging
 via the standard PostgreSQL logging facility.
 .
 The goal of pgAudit is to provide PostgreSQL users with capability to produce
 audit logs often required to comply with government, financial, or ISO
 certifications.
 .
 An audit is an official inspection of an individual's or organization's
 accounts, typically by an independent body. The information gathered by
 pgAudit is properly called an audit trail or audit log.
